#ifndef PLAYER_HPP
#define PLAYER_HPP


#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
#include <mutex>
#include <vector>



enum class Move : char {
    ROCK = 'R', PAPER = 'P', SCISSOR = 'S'
};

enum class Mode {
    DEATHMATCH, LASTMAN
};

class Player {

    std::mutex player_mutex;

public:
    int sockfd;
    struct sockaddr_in client_addr;
    socklen_t clilen;
    const static constexpr size_t NAME_SIZE = 255;
    char name[NAME_SIZE];
    short won_games;
    short num_of_games_played;
    short hp;
    bool busy;

    Player():sockfd(0),won_games(0),num_of_games_played(0),hp(0),clilen(sizeof(struct sockaddr_in)),busy(false){

        memset(&client_addr,'\0',sizeof(sockaddr_in));

        memset(name,'\0',NAME_SIZE);

    }

    void play(Player &,Mode,std::vector<Player>&,int,int);
    void player_lock  () ;
    void player_unlock ();

    ~Player();
};


#endif

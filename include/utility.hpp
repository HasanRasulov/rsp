#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <cstring>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <mutex>
#include <chrono>
#include <random>
#include <cmath>
#include <algorithm>
#include <queue>
#include <optional>
#include "player.hpp"

using namespace std::literals::string_literals;

const std::string dashes = "---------------------------------------------------";
const constexpr size_t BUF_SIZE = 255 * 1024;
static std::mutex print_lock;

template <typename O>
static O &Out(O &);

template <typename O, typename T, typename... R>
static O &Out(O &, T &&, R &&...);

template <typename... R>
std::string Color(int, R &&...);

template <bool do_lock = false, std::ostream &output = std::cout, typename... R>
void Write(R &&...);

std::string make_statistics(std::vector<Player> &, Mode);

bool send_everyone(std::vector<Player> &, std::string, short, short);

bool send_player(Player &, std::string);

bool recv_from_everyone(std::vector<Player> &players);

bool recv_player(Player &, char *, size_t);

std::optional<std::string> get_players_index(std::vector<Player> &, std::queue<std::pair<int, int>> &);

#endif

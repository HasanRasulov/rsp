#include "utility.hpp"

std::optional<std::string> get_players_index(std::vector<Player> &players, std::queue<std::pair<int, int>> &games)
{

    std::string names = "\n";

    short finish_lastman = 0;

    for (auto i = 0; i < players.size(); i++)
    {
        if (!players[i].busy && players[i].hp != 0)
        {
            finish_lastman++;
            names += std::to_string(i) + ":" + players[i].name + "\n";
        }
    }

    std::cout << "lastman:" << finish_lastman << "\n";

    if (finish_lastman == 1)
    {
        auto p = std::find_if(players.begin(), players.end(), [](const auto &p) {
            return p.hp != 0;
        });
        return std::make_optional(p->name);
    }

    for (auto i = 0; i < players.size(); i++)
    {
        if (players[i].hp == 0)
        {
            send_player(players[i], "finish");
        }
        else
            send_player(players[i], names);
    }

    char buffer[BUF_SIZE];

    int p1, p2;
    for (int i = 0; i < players.size(); i++)
    {

        if (!players[i].busy && players[i].hp != 0)
        {
            memset(buffer, '\0', BUF_SIZE);
            recv_player(players[i], buffer, BUF_SIZE);
            std::string str = buffer;
            Write("Opponenets:"s + str);
            auto pos = str.find(":");
            p1 = std::distance(players.begin(), std::find_if(players.begin(), players.end(), [&](const auto &p) {
                                   return std::string(p.name) == str.substr(pos + 1);
                               }));
            p2 = std::distance(players.begin(), std::find_if(players.begin(), players.end(), [&](const auto &p) {
                                   return std::string(p.name) == str.substr(0, pos);
                               }));

            /* don't push invalid opponents name*/
            if (p1 >= players.size() || p2 >= players.size())
                continue;

            games.push(std::make_pair(p1, p2));
        }
    }

    return std::nullopt;
}

template <typename O>
static O &Out(O &r) { return r; }

template <typename O, typename T, typename... R>
static O &Out(O &r, T &&a, R &&... rest)
{
    return Out((r << std::forward<T>(a), r), std::forward<R>(rest)...);
}

template <typename... R>
std::string Color(int n, R &&... values)
{
    std::ostringstream o;
    Out(o, n >= 0 ? "\33[38;5;" : "\33[\3\10;\5;");
    if (n >= 0)
        Out(o, n);
    else
    {
        auto t = [](int n) { return char(n % 10 ? n % 10 : 10); };
        if (n <= -100)
            Out(o, t(-n / 100));
        if (n <= -10)
            Out(o, t(-n / 10));
        Out(o, t(-n));
    }
    return Out(o, 'm', std::forward<R>(values)..., "\33[m").str();
}

//add another template param for perror
template <bool do_lock, std::ostream &output, typename... R>
void Write(R &&... values)
{

    std::unique_lock<std::mutex> l(print_lock, std::defer_lock);
    if (do_lock)
        l.lock();

    std::time_t now_c = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::ostringstream s;
    std::string line = Out(s, std::put_time(std::localtime(&now_c), "%T"), Color(37, '|'), std::forward<R>(values)...).str();
    output << line << std::endl;
}

template <>
void Write<false, std::cerr>(std::string &&msg, std::string &&file, int &&l)
{

    std::time_t now_c = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::ostringstream s;
    std::string line = Out(s, std::put_time(std::localtime(&now_c), "%T"), Color(37, '|'),
                           file + ":"s + std::to_string(l), Color(40, '|'),
                           std::forward<std::string>(msg))
                           .str();

    std::cerr << line << std::endl;
    perror("");
}

template void Write<false, std::cout>(std::string &&);
template void Write<false, std::cout>(std::string &&, std::string &&);
template void Write<true, std::cout>(std::string &&);

std::string make_statistics(std::vector<Player> &players, Mode mode)
{

    std::string res = "";

    if (mode == Mode::DEATHMATCH)
    {
        for (auto i = 0; i < players.size() && players[i].num_of_games_played != 0; i++)
        {
            //players[i].player_lock();
            res += Color(37, dashes) + "\nName:"s + std::string(players[i].name) + "\nNumber of games played:"s +
                   std::to_string(players[i].num_of_games_played) +
                   "\nWon ratio:"s +
                   std::to_string(static_cast<int>((float)players[i].won_games / (float)players[i].num_of_games_played * 100.f)) +
                   "%\n"s + Color(37, dashes) + "\n"s;
            //players[i].player_unlock();
        }
    }
    else
    {

        for (auto i = 0; i < players.size(); i++)
        {
            //players[i].player_lock();
            res += players[i].name + "\nNumber of games played:"s +
                   std::to_string(players[i].num_of_games_played) + "\nPlayer hp:"s +
                   std::to_string(players[i].hp) + "\n"s;
            //players[i].player_unlock();
        }
    }
    return res == "" ? "There has not been any game yet"s : res;
}

bool send_everyone(std::vector<Player> &players, std::string msg, short p1, short p2)
{

    for (auto i = 0; i < players.size(); i++)
    {
        //  if (players[i].hp == 0)
        //{

        //  if (!send_player(players[i], msg + "finish"s))
        //{
        //  return false;
        //}
        //}
        //else
        //{

        if (i == p1 || i == p2)
        {

            if (!send_player(players[i], msg + "start"s))
            {
                Write<false,std::cerr>("Couldn't write start message");
                return false;
            }
        }
        else
        {
            if(!send_player(players[i], msg))
            {
                Write<false,std::cerr>("Couldn't write message");
                return false;
            }
        }
        //}
    }
    return true;
}

bool send_player(Player &p, std::string msg)
{
    //p.player_lock();
    //    if (!p.busy)
    //  {
    if (write(p.sockfd, msg.c_str(), strlen(msg.c_str())) < 0)
    {
        std::cerr << Color(38, dashes);
        Write<false, std::cerr>("Error occurred while writing "s + p.name);
        std::cerr << Color(38, dashes);
        //  p.player_unlock();
        return false;
    }
    //p.player_unlock();
    return true;
    //}
    //p.player_unlock();
    //return true;
}

bool recv_from_everyone(std::vector<Player> &players)
{

    std::for_each(players.begin(), players.end(), [&](auto &p) {
        recv_player(p, p.name, Player::NAME_SIZE);
        printf("%s\n", p.name);
    });
    return true;
}

bool recv_player(Player &p, char *msg, size_t size)
{
    //p.player_lock();
    if (read(p.sockfd, msg, BUF_SIZE) < 0)
    {
        std::cerr << Color(38, dashes);
        Write<false, std::cerr>("Error occurred while writing "s + p.name);
        std::cerr << Color(38, dashes);
        return false;
    }
    //p.player_unlock();
    return true;
}

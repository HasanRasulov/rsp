#include <utility>
#include <thread>
#include "player.hpp"
#include "utility.hpp"

Player::~Player()
{

    close(sockfd);
}

void Player::play(Player &p, Mode mode, std::vector<Player> &players, int p1, int p2)
{

    Move m_p1, m_p2;
    char move_p1, move_p2;
    char buffer[BUF_SIZE];

    std::lock(this->player_mutex, p.player_mutex);
    std::lock_guard<std::mutex> mutex_p1(this->player_mutex, std::adopt_lock);
    std::lock_guard<std::mutex> mutex_p2(p.player_mutex, std::adopt_lock);

    /*check does players have at least one hp*/
    if (this->hp <= 0 || p.hp <= 0)
    {
        std::cout << "hp==0\n";
       return;
    }

    /*send stattistics and start keyword to make the client enter game loop*/
    std::string stat = make_statistics(players, mode);
    Write(std::string(stat));
    send_everyone(players, stat, p1, p2);

    this->busy = p.busy = true;
    this->num_of_games_played++;
    p.num_of_games_played++;

    recv_player(*this, &move_p1, sizeof(char));
    recv_player(p, &move_p2, sizeof(char));

    /*print p1's move*/
    std::cout << Color(38, dashes) << std::endl;
    Write<true, std::cout>(this->name + "'s move: "s + move_p1);
    std::cout << Color(38, dashes) << std::endl;

    /*print p2's move*/
    std::cout << Color(38, dashes) << std::endl;
    Write<true, std::cout>(p.name + "'s move: "s + move_p2);
    std::cout << Color(38, dashes) << std::endl;

    m_p1 = (Move)move_p1;

    m_p2 = (Move)move_p2;

    if (m_p1 == m_p2)
    {

        std::string msg = "Result is draw";

        send_player(*this, msg + ":"s + move_p2);

        send_player(p, msg + ":"s + move_p1);

        Write<true, std::cout>(std::move(msg));

        /*recv dummy string*/
        recv_player(*this, buffer, BUF_SIZE);
        /*recv dummy string*/
        recv_player(p, buffer, BUF_SIZE);

        this->busy = p.busy = false;

        return;
    }

    /*check whether input is valid move or not*/
    bool is_m_p1_valid = m_p1 == Move::ROCK || m_p1 == Move::SCISSOR || m_p1 == Move::PAPER;
    bool is_m_p2_valid = m_p2 == Move::ROCK || m_p2 == Move::SCISSOR || m_p2 == Move::PAPER;

    std::string winner_msg = "You won "s;
    std::string loser_msg = "You lost "s;

    if (!is_m_p2_valid || is_m_p1_valid && (m_p1 == Move::ROCK && m_p2 == Move::SCISSOR) ||
        (m_p1 == Move::PAPER && m_p2 == Move::ROCK) ||
        (m_p1 == Move::SCISSOR && m_p2 == Move::PAPER))
    {

        send_player(*this, winner_msg + this->name + ":"s + move_p2);

        send_player(p, loser_msg + p.name + ":"s + move_p1);

        if (mode == Mode ::DEATHMATCH)
            this->won_games++;
        else
            p.hp--;

        Write<true, std::cout>(this->name + " won"s);
    }
    else
    {
        send_player(p, winner_msg + this->name + ":"s + move_p1);

        send_player(*this, loser_msg + p.name + ":"s + move_p2);

        if (mode == Mode ::DEATHMATCH)
            p.won_games++;
        else
            this->hp--;

        Write<true, std::cout>(p.name + " won"s);
    }

    /*recv dummy string*/
    recv_player(*this, buffer, BUF_SIZE);
    /*recv dummy string*/
    recv_player(p, buffer, BUF_SIZE);

    this->busy = p.busy = false;
}

void Player::player_lock()
{

    this->player_mutex.lock();
}

void Player::player_unlock()
{
    this->player_mutex.unlock();
}

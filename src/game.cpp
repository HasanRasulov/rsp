#include <iostream>
#include <string>
#include <regex>
#include <memory>
#include <future>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "player.hpp"
#include "utility.hpp"

const int INITIAL_NUM_PLAYERS = 3;
const int INITIAL_NUM_GAMES = 3;
const int INITIAL_HP = 2;

using std::chrono_literals::operator""s;

int main(int argc, char *argv[])
{

	if (argc < 3)
	{

		std::cerr << Color(38, dashes);
		Write<false, std::cerr>("\nError syntax:./game port  deathmatch|lastman [num_players] [init_hp|num_of_games] "s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes);

		return -1;
	}

	Mode mode;
	short initial_num_games = INITIAL_NUM_GAMES, init_hp = INITIAL_HP, num_of_players = INITIAL_NUM_PLAYERS;
	std::regex num_reg("^[0-9]+$");

	if (argc > 3 && std::regex_match(argv[3], num_reg))
		num_of_players = atoi(argv[3]);

	if (std::regex_match(argv[2], std::regex("death[-,_]?(match)?", std::regex::icase)))
	{
		mode = Mode::DEATHMATCH;
		if (argc > 4 && std::regex_match(argv[4], num_reg))
			initial_num_games = atoi(argv[4]);
	}
	else if (std::regex_match(argv[2], std::regex("last[-,_]?(man)?", std::regex::icase)))
	{
		mode = Mode::LASTMAN;
		if (argc > 4 && std::regex_match(argv[4], num_reg))
			init_hp = atoi(argv[4]);
	}
	else
	{

		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Error syntax:There is no such mode(non-case sensitive )\n Death | DeathMatch | Death?match \n Last| LastMan | Last?Man \n"s,
								std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	short port;
	if (std::regex_match(std::string(argv[1]), num_reg))
		port = atoi(argv[1]);
	else
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Error syntax:Port should be number\n"s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	int sockfd;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Socket creation failed..."s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	struct timeval tv;
	tv.tv_sec = 3;
	tv.tv_usec = 0;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT | SO_RCVTIMEO, (const char *)&tv, sizeof(tv));

	std::cout << Color(38, dashes) << std::endl;
	Write("Socket has created..."s, std::to_string(sockfd));
	std::cout << Color(38, dashes) << std::endl;

	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));

	server_addr.sin_family = AF_INET;

	server_addr.sin_port = htons(port);

	server_addr.sin_addr.s_addr = INADDR_ANY;

	if (bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Couldn't bind to server... \n"s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	std::cout << Color(38, dashes) << std::endl;
	Write("Socket bound to server...\n"s);
	std::cout << Color(38, dashes) << std::endl;

	if (listen(sockfd, num_of_players) < 0)
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Couldn't listen.\n"s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	std::cout << Color(38, dashes) << std::endl;
	Write("Socket is listening...\n"s);
	std::cout << Color(38, dashes) << std::endl;

	std::vector<Player> players(num_of_players);

	for (short i = 0; i < num_of_players; i++)
	{

		if ((players[i].sockfd = accept(sockfd, (struct sockaddr *)&players[i].client_addr, &players[i].clilen)) <
			0)
		{
			std::cerr << Color(38, dashes) << std::endl;
			Write<false, std::cerr>("Couldn't accept connection\n"s, std::string(__FILE__), __LINE__);
			std::cerr << Color(38, dashes) << std::endl;
			return -1;
		}

		if (!recv_player(players[i], players[i].name, Player::NAME_SIZE - 1))
			return -1;

		players[i].hp = init_hp;

		std::cout << Color(38, dashes) << std::endl;
		Write(players[i].name + " connected"s);
		std::cout << Color(38, dashes) << std::endl;
	}

	char buffer[BUF_SIZE];
	std::queue<std::pair<int, int>> games;

	if (mode == Mode::DEATHMATCH)
	{
		for (auto i = 0; i < initial_num_games; i++)
		{

			get_players_index(players, games);

			std::vector<std::thread> game_threads;

			while (!games.empty())
			{
				auto [p1, p2] = games.front();
				std::cout << "p1:" << p1 << "---------->p2:" << p2 << std::endl;

				game_threads.push_back(std::thread(&Player::play, &players[p1], std::ref(players[p2]), mode, std::ref(players), p1, p2));
				Write(players[p1].name + " is going to play with "s + players[p2].name);

				games.pop();
			}

			for (auto &t : game_threads)
			{
				t.join();
			}

			std::string stat = make_statistics(players, mode);
			Write(std::string(stat));
			stat += "end";
			send_everyone(players, stat, -1, -1);
			/* gap between two send function*/
			std::this_thread::sleep_for(2s);
		}

		std::string stat = make_statistics(players, mode);
		Write(std::string(stat));
		stat += "finish";
		send_everyone(players, stat, -1, -1);
	}
	else if (mode == Mode::LASTMAN)
	{
		std::optional<std::string> lastman;

		while (true)
		{
			lastman = get_players_index(players, games);

			if (lastman.has_value())
			{
				std::cout<<"lastman value:"<<lastman.value();
				break;
			}

			std::vector<std::thread> game_threads;

			while (!games.empty())
			{
				const auto& [p1, p2] = games.front();
				std::cout << "p1:" << p1 << "---------->p2:" << p2 << std::endl;
                
				game_threads.push_back(std::thread(&Player::play, &players[p1], std::ref(players[p2]), mode, std::ref(players), p1, p2));
				Write(players[p1].name + " is going to play with "s + players[p2].name);

				games.pop();
			}

			for (auto &t : game_threads)
			{
				t.join();
			}

			std::string stat = make_statistics(players, mode);
			Write("Round over:\n"s + std::string(stat));
			stat += "end";
			send_everyone(players, stat, -1, -1);

			/* gap between two send function*/
			std::this_thread::sleep_for(2s);
		}

		//std::cout << "Winner:" << lastman.value() << std::endl;

		std::string stat = make_statistics(players, mode);
		stat += "\nWinner:" + lastman.value();
		Write("Game finished:\n"s + std::string(stat));
		stat += "finish";
		send_everyone(players, stat, -1, -1);
	}

	close(sockfd);
	return 0;
}

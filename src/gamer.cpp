#include <unistd.h>
#include <cstring>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <regex>
#include <thread>

#include "player.hpp"
#include "utility.hpp"

using namespace std::chrono_literals;

int main(int argc, char *argv[])
{

	if (argc < 2)
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Error syntax:./gamer [host] port \n "s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	const char *_port, *_host;

	if (argc == 2)
	{
		_port = argv[1];
		_host = "127.0.0.1";
	}
	else if (argc == 3)
	{
		_host = argv[1];
		_port = argv[2];
	}

	short port;
	if (std::regex_match(std::string(_port), std::regex("^[0-9]*$")))
		port = atoi(_port);

	else
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Error syntax:Port should be number:"s + std::to_string(port), std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	int sockfd;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Socket creation failed...\n"s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	std::cout << Color(38, dashes) << std::endl;
	Write("Socket has created...\n"s);
	std::cout << Color(38, dashes) << std::endl;

	struct hostent *server;

	if ((server = gethostbyname(_host)) == NULL)
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("No such host...:"s + std::string(_host), std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}

	struct sockaddr_in serv_addr;

	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	memcpy(server->h_addr, &serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(port);

	if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
	{
		std::cerr << Color(38, dashes) << std::endl;
		Write<false, std::cerr>("Connection has failed...\n"s, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes) << std::endl;
		return -1;
	}
	std::cout << Color(38, dashes) << std::endl;
	Write("Connection has created...\n"s);
	std::cout << Color(38, dashes) << std::endl;

	Player p;

	printf("Enter your name:");

	scanf("%s", p.name);

	/*send name*/
	if (write(sockfd, p.name, strlen(p.name)) < 0)
	{
		std::cerr << Color(38, dashes);
		Write<false, std::cerr>("Error occurred while writing "s + p.name, std::string(__FILE__), __LINE__);
		std::cerr << Color(38, dashes);
	}

	Write("Connected to ... Host:"s + std::string(_host) + Color(40, '|') + "Port:"s + std::string(_port));

	char buffer[BUF_SIZE];

	while (true)
	{

		memset(buffer, '\0', BUF_SIZE);
        /* read opponents*/  
		if (read(sockfd, buffer, BUF_SIZE - 1) < 0)
		{
			std::cerr << Color(38, dashes);
			Write<false, std::cerr>("Error occurred while reading "s, std::string(__FILE__), __LINE__);
			std::cerr << Color(38, dashes);
		}

		std::string stat = buffer;
         
		if (std::regex_search(stat, std::regex("finish")))
		{
			stat = std::regex_replace(stat, std::regex("finish"), "");
			Write<false, std::cerr>("The final statistics:\n"s + stat);
			break;
		}

		std::string opponents = std::regex_replace(stat, std::regex("[0-9]:"s + p.name), "");
		Write("Opponents:\n" + std::move(opponents));
		Write("Choose opponent name:"s);

		memset(buffer, '\0', BUF_SIZE);
		scanf("%s", buffer);
		
		/*chosen opponent name to player name*/
		strncat(buffer, ":", 1);
		strncat(buffer, p.name, strlen(p.name));

		Write(std::string(buffer));

		/*send opponent*/
		if (write(sockfd, buffer, strlen(buffer)) < 0)
		{
			std::cerr << Color(38, dashes);
			Write<false, std::cerr>("Error occurred while writing "s + p.name, std::string(__FILE__), __LINE__);
			std::cerr << Color(38, dashes);
		}

		while (true)
		{
			memset(buffer, '\0', BUF_SIZE);

            /*read statistics and start keyword*/ 
			if (read(sockfd, buffer, BUF_SIZE - 1) < 0)
			{
				std::cerr << Color(38, dashes);
				Write<false, std::cerr>("Error occurred while reading..."s, std::string(__FILE__), __LINE__);
				std::cerr << Color(38, dashes);
				//return -1;
			}

			bool end = std::regex_search(std::string(buffer), std::regex("end"));
			bool start = std::regex_search(std::string(buffer), std::regex("start"));

			std::string stat = buffer;
			/*Get and display players' statistics*/
			if (start | end)
				stat = std::regex_replace(stat, std::regex("start|end"), "");

			if (end)
			{
				Write<false, std::cerr>("The round  statistics:\n"s + stat);
				break;
			}

			if (start)
			{

				memset(buffer, '\0', BUF_SIZE);

				/*Enter the move and send to server*/

				Write("Enter move "s + p.name + ":(R|P|S)"s);
				scanf("%s", buffer);

				if (write(sockfd, buffer, strlen(buffer)) < 0)
				{
					std::cerr << Color(38, dashes);
					Write<false, std::cerr>("Error occurred while writing "s + p.name, std::string(__FILE__), __LINE__);
					std::cerr << Color(38, dashes);
				}

				memset(buffer, '\0', BUF_SIZE);

				/*get opponent's move and display and result */

				if (read(sockfd, buffer, BUF_SIZE - 1) < 0)
				{
					std::cerr << Color(38, dashes);
					Write<false, std::cerr>("Error occurred while reading "s, std::string(__FILE__), __LINE__);
					std::cerr << Color(38, dashes);
				}

				std::string buf = buffer;
				auto pos = buf.find(":");

				Write("The opponent's move : "s, buf.substr(pos + 1));

				std::cout << Color(38, dashes) << std::endl;
				Write("Result of last game:"s, buf.substr(0, pos));

				std::cout << Color(38, dashes) << std::endl;
				Write("The current statistics:\n"s, std::move(stat));

				/*send dummy string to have gap between two read function*/

				memset(buffer, '\0', BUF_SIZE);
				strcpy(buffer, "I got result:");
				strcat(buffer, p.name);

				if (write(sockfd, buffer, strlen(buffer)) < 0)
				{
					std::cerr << Color(38, dashes);
					Write<false, std::cerr>("Error occurred while writing "s + p.name, std::string(__FILE__), __LINE__);
					std::cerr << Color(38, dashes);
				}
			}
			else
			{
				std::this_thread::sleep_for(3s);
				//if (stat.empty())
				//	return 0;
				Write<false, std::cerr>("The current statistics(wait...):\n"s + stat);
			}
		}
	}


	close(sockfd);

	return 0;
}
